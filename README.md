# Adonis API application

https://adonisjs.com/docs/4.1/installation

## Step

rename .env.example to .env

```bash
npm install
adonis serve --dev
```

### Route
```bash
/first-challenge
/second-challenge
/third-challenge
```

### Run Test

I have created unit test for all of challenges 

```js
adonis test
```
