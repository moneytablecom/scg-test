'use strict'

class TestController {
    
    first () {
        let number = [3, 5]

        for (let index = 0; index < 3; index++) {
          number.push(number[index] * 3)
        }

        return {
          answer: number
        }
    }

    second() {
        let y = Math.abs(24 + (10 * 2) - 99)
        return {
          answer: y
        }

    }

    third() {
        let number = ['5'];
        let key = 2;

        for (let index = 0; index < 4; index++) {
          number.push(`${key}${number[index]}`);
          key++
        }

        return {
          answer: number

        }
    }
}

module.exports = TestController
