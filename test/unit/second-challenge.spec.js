'use strict'

const { test } = use('Test/Suite')('Second Challenge')

test('make sure (Y + 24)+(10 × 2) = 99 Y is 55 ', async ({
      assert
    }) => {
  
  let y = Math.abs(24 + (10 * 2) - 99)
  
  assert.equal(y, 55)
})
