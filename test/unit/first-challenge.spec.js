'use strict'

const { test } = use('Test/Suite')('First Challenge')

test('make sure 3, 5, 9, 15, X is 27', async ({
      assert
    }) => {
  
  let number = [3,5]

  for (let index = 0; index < 3; index++) {   
     number.push(number[index] * 3)
  }

  assert.isArray(number, [3, 5, 9, 15, 27])

})
