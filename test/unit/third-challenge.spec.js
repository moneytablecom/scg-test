'use strict'

const { test } = use('Test/Suite')('Third Challenge')

test('make sure if 1 = 5 , 2 = 25 , 3 = 325 , 4 = 4325 Then 5 = 54325 ', async ({
      assert
    }) => {

  let number = ['5'];
  let key = 2;

  for (let index = 0; index < 4; index++) {
    number.push(`${key}${number[index]}`);
    key++
  }

  assert.isArray(number, ['5','25','325','4325','54325'])

})
